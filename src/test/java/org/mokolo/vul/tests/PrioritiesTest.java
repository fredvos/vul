package org.mokolo.vul.tests;

import static org.junit.Assert.*;

import org.junit.Test;
import org.mokolo.vul.Priorities;

public class PrioritiesTest {
  private static Priorities priorities;
  
  static {
    priorities = new Priorities();
    priorities.add("Tinariwen/Tassili", 2.0);
    priorities.add("Tinariwen", 1.5);
    priorities.add("[Aa]fri[ck]a", 1.0);
  }

  @Test
  public void canMatchTinariwenAlbums() {
    assertEquals("Found Tassili", 0, priorities.getPatternIndex("Tinariwen/Tassili"));
    assertEquals("Found Tinariwen", 1, priorities.getPatternIndex("Tinariwen/Emmaar"), 0.001);
  }

  @Test
  public void canMatchAfricanStuff() {
    assertEquals("Found Afrika", 2, priorities.getPatternIndex("Various_Artists/Afrika_Moyo"), 0.001);
    assertEquals("Found Africa", 2, priorities.getPatternIndex("Various_Artists/Rough_Guide_To_West_Africa"), 0.001);
  }

  @Test
  public void canHandleNotFound() {
    assertEquals("Not found", -1, priorities.getPatternIndex("Earth_and_Fire/Gate_to_Infinity"));
  }
}
