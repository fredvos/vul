package org.mokolo.vul.tests;

import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mokolo.vul.Directory;
import org.mokolo.vul.IsCommentException;
import org.mokolo.vul.ParsingException;

public class DirectoryTest {

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Test
  public void canParseSimpleLine() throws Exception {
    String s = "12 50791042 The_Breeders/Pod";
    Directory d = Directory.parseLine(s);
    assertEquals("Directory matches", "The_Breeders/Pod", d.directory);
    assertEquals("Music file count matches", 12, d.fileCount);
    assertEquals("Size estimate matches", 50791042L, d.sizeEstimateBytes);
  }

  @Test
  public void canParseLineWithWhitespaceInDirectoryName() throws Exception {
    String s = "10 105730017 The Velvet Underground/The Velvet Underground";
    Directory d = Directory.parseLine(s);
    assertEquals("Directory matches", "The Velvet Underground/The Velvet Underground", d.directory);
  }

  @Test
  public void emptyLineThrowsException() throws Exception {
    thrown.expect(IsCommentException.class);
    
    String s = "";
    Directory.parseLine(s);
  }

  @Test
  public void commentLineThrowsException() throws Exception {
    thrown.expect(IsCommentException.class);
    
    String s = "# This is a comment";
    Directory.parseLine(s);
  }

  @Test
  public void notWellformedLineThrowsException() throws Exception {
    thrown.expect(ParsingException.class);
    
    String s = "1 2";
    Directory.parseLine(s);
  }
  
  @Test
  public void toStringFormatsDirectory() {
    Directory d = new Directory();
    d.directory = "The_Breeders/Pod";
    d.fileCount = 12;
    d.sizeEstimateBytes = 50791042L;
    assertEquals("ToString works", "12 50791042 The_Breeders/Pod", d.toString());
  }

}
