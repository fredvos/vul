package org.mokolo.vul.tests;

import java.io.File;

import org.junit.*;
import org.junit.rules.ExpectedException;
import org.mokolo.vul.Collection;
import org.mokolo.vul.Directory;

import static org.junit.Assert.*;

public class CollectionTest {
  
  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Test
  public void canAnalyzeCollection() throws Exception {
    File tempDirectory = File.createTempFile("vul", null);
    tempDirectory.delete();
    tempDirectory.mkdir();
    
    Collection collection = new Collection("test", tempDirectory);

    String pathToBaseDirectory = "src/test/resources/source";
    File baseDirectory = new File(pathToBaseDirectory.replaceAll("/", File.separator));
    collection.addBaseDirectory(baseDirectory);

    collection.index();
    
    assertNotNull("Has collection", collection.getDirectoriesCollection());
    assertEquals("Has 5 directories with music files", 5, collection.getDirectoriesCollection().size());
    
    assertNotNull("Has map", collection.getDirectoriesMap());
    
    Directory emmaar = collection.getDirectoriesMap().get("Tinariwen/Emmaar");
    assertNotNull("Found Emmaar", emmaar);
    assertEquals("Emmaar has 11 tracks (and one image)", 11, emmaar.fileCount);
  }
}
