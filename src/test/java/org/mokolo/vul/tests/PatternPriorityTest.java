package org.mokolo.vul.tests;

import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mokolo.vul.IsCommentException;
import org.mokolo.vul.ParsingException;
import org.mokolo.vul.PatternPriority;

public class PatternPriorityTest {

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Test
  public void canParseSimpleLine() throws Exception {
    String s = "1.5 Tinariwen";
    PatternPriority pp = PatternPriority.parseLine(s);
    assertEquals("Priority matches", 1.5, pp.priority, 0.001);
    assertEquals("Pattern matches", "Tinariwen", pp.pattern.toString());
  }

  @Test
  public void canParseLineWithWhitespaceInPattern() throws Exception {
    String s = "-1.0 Deep\\ Purple";
    PatternPriority pp = PatternPriority.parseLine(s);
    assertEquals("Priority matches", -1.0, pp.priority, 0.001);
    assertEquals("Pattern matches", "Deep\\ Purple", pp.pattern.toString());
  }

  @Test
  public void canParseLineWithPatternEndingInSlash() throws Exception {
    String s = "-0.5 Glass/";
    PatternPriority pp = PatternPriority.parseLine(s);
    assertEquals("Priority matches", -0.5, pp.priority, 0.001);
    assertEquals("Pattern matches", "Glass/", pp.pattern.toString());
  }

  @Test
  public void emptyLineThrowsException() throws Exception {
    thrown.expect(IsCommentException.class);
    
    String s = "";
    PatternPriority.parseLine(s);
  }

  @Test
  public void commentLineThrowsException() throws Exception {
    thrown.expect(IsCommentException.class);
    
    String s = "# This is a comment";
    PatternPriority.parseLine(s);
  }

  @Test
  public void notWellformedLineThrowsException() throws Exception {
    thrown.expect(ParsingException.class);
    
    String s = "1 bla";
    PatternPriority.parseLine(s);
  }
  
}
