package org.mokolo.vul;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Directory implements Comparable<Directory> {
  public String directory;
  public int fileCount;
  public long sizeEstimateBytes;
  
  private static Pattern directoryPattern = Pattern.compile("^(\\d+)\\s+(\\d+)\\s+(.*)$");
  
  public static Directory parseLine(String s)
  throws IsCommentException, ParsingException {
    Directory directory = new Directory();
    Matcher matcher = directoryPattern.matcher(s);
    if (matcher.find()) {
      directory.fileCount = Integer.parseInt(matcher.group(1));
      directory.sizeEstimateBytes = Long.parseLong(matcher.group(2));
      directory.directory = matcher.group(3);
    }
    else
      if (s.length() == 0 || s.charAt(0) == '#')
        throw new IsCommentException();
      else
        throw new ParsingException("Does not look like a directory line");
    return directory;
  }

  @Override
  public String toString() {
    return String.format("%d %d %s", this.fileCount, this.sizeEstimateBytes, this.directory);
  }

  public int compareTo(Directory d) {
    int retval = this.directory.compareTo(d.directory);
    if (retval == 0)
      retval = this.fileCount - d.fileCount;
    if (retval == 0)
      retval = (int) (this.sizeEstimateBytes - d.sizeEstimateBytes);
    return retval;
  }
}
