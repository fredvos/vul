package org.mokolo.vul;

public class ParsingException extends Exception {
  private static final long serialVersionUID = -155677019785538391L;
  public ParsingException(String msg) {
    super(msg);
  }
}
