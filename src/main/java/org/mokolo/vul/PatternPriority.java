package org.mokolo.vul;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternPriority {
  public Pattern pattern;
  public double priority;
  
  private static Pattern patternPriorityPattern = Pattern.compile("^(\\-??\\d+\\.\\d+)\\s+(.*)$");
  
  private PatternPriority() {
  }
  
  public PatternPriority(String patternString, double priority) {
    this.pattern = Pattern.compile(patternString);
    this.priority = priority;
  }
  
  public static PatternPriority parseLine(String s)
  throws IsCommentException, ParsingException {
    PatternPriority pp = null;
    Matcher matcher = patternPriorityPattern.matcher(s);
    if (matcher.find()) {
      pp = new PatternPriority();
      pp.priority = Double.parseDouble(matcher.group(1));
      pp.pattern = Pattern.compile(matcher.group(2));
    }
    else
      if (s.length() == 0 || s.charAt(0) == '#')
        throw new IsCommentException();
      else
        throw new ParsingException("Does not look like a priority line");
    return pp;
  }
}
