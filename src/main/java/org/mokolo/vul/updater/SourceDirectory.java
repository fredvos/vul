package org.mokolo.vul.updater;

import org.mokolo.vul.Directory;

public class SourceDirectory {
  public Directory directory;
  public String collectionId;
  public double priority;
}
