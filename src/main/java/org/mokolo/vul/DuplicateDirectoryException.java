package org.mokolo.vul;

public class DuplicateDirectoryException extends Exception {
  private static final long serialVersionUID = -6459434168213761266L;
  public DuplicateDirectoryException(String msg) {
    super(msg);
  }
}
