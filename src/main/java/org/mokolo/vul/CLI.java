package org.mokolo.vul;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import joptsimple.OptionParser;
import joptsimple.OptionSet;

import org.apache.log4j.Logger;
import org.mokolo.commons.command.CommandDefinition;
import org.mokolo.commons.command.CommandParser;
import org.mokolo.commons.lang.Log4JConfiguration;
import org.mokolo.commons.lang.document.Document;

public class CLI {

  private static Logger logger = Logger.getLogger(CLI.class);
  
  public static void main(String[] args) {
    try {
      CommandParser commandParser = new CommandParser(new OptionParser("v"), "request");
      commandParser.addCommandDefinition(new CommandDefinition("add-collection", null)
          .addRequiredArgument("id")
          .addRequiredArgument("type")
          .addRequiredArgument("base-directory")
          .addOptionalArgument("check-filename")
          .addOptionalArgument("max-size"));
      commandParser.addCommandDefinition(new CommandDefinition("add-base-directory", null)
          .addRequiredArgument("id")
          .addRequiredArgument("base-directory"));
      commandParser.addCommandDefinition(new CommandDefinition("list", null));
      commandParser.addCommandDefinition(new CommandDefinition("index", null).addRequiredArgument("id"));
      commandParser.addCommandDefinition(new CommandDefinition("analyze-priorities", null).addRequiredArgument("id"));
      commandParser.addCommandDefinition(new CommandDefinition("update", null));
      OptionSet options = commandParser.getOptionParser().parse(args);

      Log4JConfiguration log4j = new Log4JConfiguration();
      Log4JConfiguration.Level level = Log4JConfiguration.Level.NORMAL;
      if (options.has("v"))
        level = Log4JConfiguration.Level.VERBOSE;
      log4j.addConsoleAppender(level, Log4JConfiguration.NONE);
      log4j.addLogger("org.mokolo.vul", level);
      log4j.addLogger("org.mokolo.commons", level);

      File homeDirectory = new File(System.getProperty("user.home"));
      File vulDirectory = new File(homeDirectory, ".vul");
      String configuration = "default";
      File configDirectory = new File(vulDirectory, configuration);

      Document document = commandParser.parseCommandWithSwitches(options);
      commandParser.validateDocument(document);
      
      if (document.getValue("request").equals("add-collection")) {
        /*
         * Creates new properties file <id>.collection in configDirectory.
         * 
         * File contains at least entry 'base-directories' and optional
         * entries 'check-filename' and 'max-size'.
         */
        String id = document.getValue("id");
        Collection collection = new Collection(id, configDirectory);
        if (collection.isStored())
          throw new AlreadyExistsException("Collection with id '"+id+"' already exists. Cannot create a new one.");
        String type = document.getValue("type");
        if (! type.equals("s") && ! type.equals("t"))
          throw new Exception("Invalid parameter; --type = s|t");
        if (type.equals("t"))
          collection.setType(Collection.Type.TARGET);
        else
          collection.setType(Collection.Type.SOURCE);
        File baseDirectory = new File(document.getValue("base-directory"));
        collection.addBaseDirectory(baseDirectory);
        String checkFilename = document.getValue("check-filename");
        if (checkFilename != null)
          collection.setCheckFilename(checkFilename);
        String maxSize = document.getValue("max-size");
        if (maxSize != null)
          collection.setMaxSize(new Long(maxSize));
        collection.save();
      }
      else if (document.getValue("request").equals("add-base-directory")) {
        /*
         * Adds a directory to entry 'base-directories' in
         * <id>.collection properties file in configDirectory.
         * Uses comma as separator.
         */
        String id = document.getValue("id");
        Collection collection = Collection.loadCollection(id, configDirectory);
        File baseDirectory = new File(document.getValue("base-directory"));
        collection.addBaseDirectory(baseDirectory);
        collection.save();
      }
      else if (document.getValue("request").equals("list")) {
        /*
         * Lists current status.
         * 
         * ID = id of collection
         * Max size = either max-size set in <id>.collection properties file
         *            or computed max size if not set. Computed size =
         *            0.95 * (size collection + free space on device)
         * Index timestamp = time of last index
         * Dirs = number of directories containing music files
         *        (= number of lines in index file)
         * Size = total size of all files in directories containing music files
         * Base directory = found base directory
         */
        List<String> ids = getSortedList(configDirectory);
        System.out.println();
        System.out.println("COLLECTIONS");
        System.out.println();
        if (ids.size() > 0) {
          System.out.println("ID               T Max size Index timestamp      Dirs Size     Base directory");
          System.out.println("---------------- - -------- ------------------- ----- -------- --------------");
        }
        for (String id : ids) {
          Collection collection = Collection.loadCollection(id, configDirectory);
          System.out.print(String.format("%-16s %1s %-8s ", id, collection.getTypeString(), Collection.humanReadableByteCount(collection.getMaxSize(), true)));
          if (collection.getIndexTimestamp() != null)
            System.out.print(Collection.timeFormat.format(collection.getIndexTimestamp()));
          else
            System.out.print("                   ");
          System.out.print(" ");
          if (collection.getIndexDirectoryCount() != null)
            System.out.print(String.format("%5d ", collection.getIndexDirectoryCount()));
          else
            System.out.print("      ");
          if (collection.getIndexSize() != null)
            System.out.print(String.format("%-8s ", Collection.humanReadableByteCount(collection.getIndexSize(), true)));
          else
            System.out.print("         ");
          try {
            File baseDirectory = collection.getBaseDirectory();
            System.out.print(baseDirectory.getAbsolutePath());
          } catch (FileNotFoundException e) {
            System.out.print("N o t   f o u n d");
          }
          System.out.println();
        }
        System.out.println();
      }
      else if (document.getValue("request").equals("index")) {
        /*
         * Generates file <id>.index in configDirectory containing list of
         * directories that contain music files.
         * Index file is a text file with three 'columns':
         * - number of music files (.mp3, .ogg, .flac, etc) in that directory
         * - total size in bytes of all files in that directory
         * - directory name
         * Updates file <id>.collection in same directory setting or updating
         * entries 'index-timestamp', 'index-directory-count', 'index-size'.
         */
        String id = document.getValue("id");
        Collection collection = Collection.loadCollection(id, configDirectory);
        collection.index();
      } 
      else if (document.getValue("request").equals("analyze-priorities")) {
        /*
         * Checks index against priorities.
         */
        String id = document.getValue("id");
        Collection collection = Collection.loadCollection(id, configDirectory);
        Priorities priorities = new Priorities();
        File prioritiesFile = new File(configDirectory, "priorities");
        priorities.load(prioritiesFile);
        PatternPriorityAnalyzis analyzis = collection.getPrioritiesAnalysis(priorities);
        System.out.println();
        System.out.println("  # Count   Prio Pattern                  First matching directory");
        System.out.println("--- ----- ------ ------------------------ ------------------------");

        for (int i=0; i<analyzis.size()-1; i++) {
          PatternPriorityStatistics stats = analyzis.get(i);
          System.out.println(String.format("%3d %5d %6.2f %-24s %s" , i+1, stats.count, stats.patternPriority.priority, stats.patternPriority.pattern.toString(), ifNull(stats.firstMatch, "")));
        }
        PatternPriorityStatistics stats = analyzis.get(-1);
        // No match:        
        System.out.println(String.format("    %5d %6.2f %-24s %s", stats.count, stats.patternPriority.priority, stats.patternPriority.pattern.toString(), stats.firstMatch));
        System.out.println();
      } 
      else if (document.getValue("request").equals("update")) {
        Updater updater = new Updater(configDirectory);

        List<String> ids = getSortedList(configDirectory);
        for (String id : ids) {
          Collection collection = Collection.loadCollection(id, configDirectory);
          collection.loadIndex();
          updater.addCollection(collection);
        }
        updater.update();
      } 
    } catch (DuplicateDirectoryException e) {
      logger.warn(e.getMessage());
    } catch (InconsistentDirectoryException e) {
      logger.error(e.getMessage());
    } catch (Exception e) {
      logger.error(e.getClass().getName()+" occurred. Msg="+e.getMessage());
      e.printStackTrace();
    }

  }

  private static List<String> getSortedList(File configDirectory) {
    List<String> ids = new ArrayList<String>();
    File[] files = configDirectory.listFiles();
    for (File file : files) {
      if (file.isFile() && file.getName().endsWith(".collection"))
        ids.add(file.getName().substring(0, file.getName().length()-11));
    }
    Collections.sort(ids);
    return ids;
  }

  private static String ifNull(String s, String then) {
    if (s != null)
      return s;
    else
      return then;
  }
}
