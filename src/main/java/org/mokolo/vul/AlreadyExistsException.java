package org.mokolo.vul;

public class AlreadyExistsException extends Exception {
  private static final long serialVersionUID = 2310255008987626176L;

  public AlreadyExistsException(String msg) {
    super(msg);
  }
}
