package org.mokolo.vul;

public class ListEntry {
  String directory;
  long sizeEstimateBytes;
  double priority;
  String sourceCollection;
  String targetCollection;
}
