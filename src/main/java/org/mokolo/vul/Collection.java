package org.mokolo.vul;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FileSystemUtils;
import org.apache.log4j.Logger;

public class Collection {
  public enum Type { SOURCE, TARGET };

  private String id;
  private Type type;
  private File configDirectory;
  private List<File> baseDirectories;
  private String checkFilename;
  private Long maxSize;
  
  private List<Directory> directoriesCollection;
  private Map<String, Directory> directoriesMap;
  
  // Status
  private Date indexTimestamp;
  private Integer indexDirectoryCount;
  private Long indexSize;

  public static SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  private static Logger logger = Logger.getLogger(Collection.class);
  
  public Collection(String id, File configDirectory) {
    this.id = id;
    this.configDirectory = configDirectory;
    this.baseDirectories = new ArrayList<File>();
    this.directoriesCollection = new ArrayList<Directory>();
    this.directoriesMap = new HashMap<String, Directory>();
  }
  
  public static Collection loadCollection(String id, File configDirectory) throws IOException {
    Collection collection = new Collection(id, configDirectory);
    Properties properties = collection.loadProperties();
    
    String typeString = properties.getProperty("type");
    if (typeString.equals("t"))
      collection.setType(Type.TARGET);
    else
      collection.setType(Type.SOURCE);
    for (String baseDirectoryPath : properties.getProperty("base-directories").split(",")) {
      File baseDirectory = new File(baseDirectoryPath);
      collection.addBaseDirectory(baseDirectory);
    }
    String checkFilename = properties.getProperty("check-filename");
    if (checkFilename != null && checkFilename.length() > 0)
      collection.setCheckFilename(checkFilename);
    String maxSize = properties.getProperty("max-size");
    if (maxSize != null && maxSize.length() > 0)
      collection.setMaxSize(new Long(maxSize));
    
    String s;
    s = properties.getProperty("index-timestamp");
    if (s != null && s.length() > 0) {
      try {
        collection.indexTimestamp = timeFormat.parse(s);
      } catch (ParseException e) {
        logger.error(e.getMessage());
      }
    }
    s = properties.getProperty("index-directory-count");
    if (s != null && s.length() > 0)
      collection.indexDirectoryCount = new Integer(s);
    s = properties.getProperty("index-size");
    if (s != null && s.length() > 0)
      collection.indexSize = new Long(s);
    
    return collection;
  }
  
  private Properties loadProperties() throws IOException {
    File propertiesFile = this.getPropertiesFile();
    InputStream is = new FileInputStream(propertiesFile);
    Properties properties = new Properties();
    properties.load(is);
    is.close();
    return properties;
  }

  public void save() throws IOException {
    Properties properties = new Properties();
    properties.put("type", this.getTypeString());
    StringBuffer buf = new StringBuffer();
    for (File baseDirectory : this.baseDirectories) {
      buf.append(",");
      buf.append(baseDirectory.getAbsolutePath());
    }
    properties.setProperty("base-directories", buf.substring(1));
    if (this.checkFilename != null)
      properties.setProperty("check-filename", this.checkFilename);
    if (this.indexTimestamp != null)
      properties.setProperty("index-timestamp", timeFormat.format(this.indexTimestamp));
    if (this.indexDirectoryCount != null)
      properties.setProperty("index-directory-count", this.indexDirectoryCount.toString());
    if (this.indexSize != null)
      properties.setProperty("index-size", this.indexSize.toString());
    
    File file = this.getPropertiesFile();
    OutputStream os = new FileOutputStream(file);
    properties.store(os, "Created by vul");
    os.close();
  }
  
  public String getId() {
    return this.id;
  }
  
  public Type getType() {
    return this.type;
  }
  
  public String getTypeString() {
    if (this.getType() == Collection.Type.TARGET)
      return "t";
    else
      return "s";
  }
  
  public void setType(Type type) {
    this.type = type;
  }
  
  public void addBaseDirectory(File baseDirectory) {
    this.baseDirectories.add(baseDirectory);
  }
  
  public void setCheckFilename(String filename) {
    this.checkFilename = filename;
  }
  
  /**
   * Returns maxSize if set, otherwise 0.95 * (size + free space)
   */
  public long getMaxSize() {
    if (this.maxSize != null)
      return this.maxSize;
    else {
      long computedMaxSize = 0L;
      if (this.indexSize != null)
        computedMaxSize += this.indexSize;
      try {
        String baseDirectoryString = this.getBaseDirectory().getAbsolutePath();
        long freeSpace = 1024L * FileSystemUtils.freeSpaceKb(baseDirectoryString, 20000L);
        computedMaxSize += freeSpace;
      } catch (IOException e) {}
      return (long) (computedMaxSize * 0.95);
    }
  }
  
  public void setMaxSize(Long maxSize) {
    this.maxSize = maxSize;
  }

  public List<Directory> getDirectoriesCollection() {
    return this.directoriesCollection;
  }
  
  public Map<String, Directory> getDirectoriesMap() {
    return this.directoriesMap;
  }
  
  public boolean isStored() {
    File file = this.getPropertiesFile();
    return file.exists();
  }
  
  public Date getIndexTimestamp() {
    return this.indexTimestamp;
  }
  
  public Integer getIndexDirectoryCount() {
    return this.indexDirectoryCount;
  }
  
  public Long getIndexSize() {
    return this.indexSize;
  }
  
  public File getBaseDirectory() throws FileNotFoundException {
    File baseDirectory = null;
    if (this.baseDirectories == null || this.baseDirectories.size() == 0)
      throw new FileNotFoundException("No paths provided");
    if (this.checkFilename == null)
      baseDirectory = this.baseDirectories.get(0);
    else {
      boolean foundIt = false;
      for (File directory : this.baseDirectories) {
        if (directory.exists() && directory.isDirectory()) {
          File checkFile = new File(directory, this.checkFilename);
          if (checkFile.exists()) {
            baseDirectory = directory;
            foundIt = true;
            break;
          }
        }
      }
      if (foundIt == false)
        throw new FileNotFoundException("Did not find candidate directory");
    }
    return baseDirectory;
  }

  private File getPropertiesFile() {
    File file = new File(this.configDirectory, id+".collection");
    return file;
  }

  public void index() throws IOException {
    logger.info("Indexing collection '"+this.getId()+"'");
    this.collectDirectoriesData();
    this.generateNewIndexFile();
    this.updateRedundantIndexData();
    this.save();
    logger.info("Done");
  }

  public void loadIndex() throws IOException {
    this.loadIndexFile();
    this.updateRedundantIndexData();
  }

  private void updateRedundantIndexData() {
    this.indexTimestamp = new Date();
    this.indexDirectoryCount = this.directoriesCollection.size();
    this.indexSize = new Long(this.computeIndexSize());
  }
  
  private void collectDirectoriesData() {
    try {
      DirectoryCollector collector = new DirectoryCollector(this.getBaseDirectory());
      this.directoriesCollection = collector.collect();
      this.directoriesMap = new HashMap<String, Directory>();
      for (Directory directory : this.directoriesCollection) {
        this.directoriesMap.put(directory.directory, directory);
      }
    } catch (IOException e) {
      logger.error(e.getClass().getName()+" occurred. Msg = "+e.getMessage());
    }
  }
  
  private void generateNewIndexFile() throws IOException {
    File indexFile = this.getIndexFile();
    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(indexFile)));
    for (Directory dir : directoriesCollection)
      out.println(dir);
    out.close();
  }
  
  private void loadIndexFile() throws IOException {
    this.directoriesCollection.clear();
    File indexFile = this.getIndexFile();
    BufferedReader reader = new BufferedReader(new FileReader(indexFile));
    String line;
    while((line = reader.readLine()) != null) {
      try {
        line = line.trim();
        Directory directory = Directory.parseLine(line);
        this.directoriesCollection.add(directory);
      } catch (IsCommentException e) {
        // Ignore empty lines and lines starting with '#'
      } catch (org.mokolo.vul.ParsingException e) {
        logger.error("Cannot parse line '"+line+"'as directory. Msg="+e.getMessage());
      }
    }
    reader.close();
  }
  
  private File getIndexFile() {
    File file = new File(this.configDirectory, id+".index");
    return file;
  }
  
  private long computeIndexSize() {
    long size = 0L;
    for (Directory directory : directoriesCollection)
      size += directory.sizeEstimateBytes;
    return size;
  }
  
  /**
   * Generates statistics about matches for patterns for this collection.
   */
  public PatternPriorityAnalyzis getPrioritiesAnalysis(Priorities priorities) {
    if (this.directoriesCollection == null || this.directoriesCollection.size() == 0) {
      try {
        this.loadIndex();
      } catch (IOException e) {
        logger.error(e.getClass().getName()+" occurred. Msg="+e.getMessage());
      }
    }
    PatternPriorityAnalyzis map = new PatternPriorityAnalyzis();
    PatternPriorityStatistics pps = new PatternPriorityStatistics();
 
    /*
     * Create 'empty' map:
     */
    
    // The no-match entry:
    pps.count = 0;
    pps.patternPriority = new PatternPriority(".*", 0.0);
    map.put(-1,  pps);
    
    // The match enties:
    int key = 0;
    for (PatternPriority pp : priorities) {
      pps = new PatternPriorityStatistics();
      pps.patternPriority = pp;
      pps.count = 0;
      map.put(key, pps);
      key++;
    }
    
    /*
     * Analyze:
     */
    for (Directory directory : this.directoriesCollection) {
      int i = priorities.getPatternIndex(directory.directory);
      PatternPriorityStatistics stats = map.get(i);
      if (stats.firstMatch == null) {
        stats.firstMatch = directory.directory;
      }
      stats.count++;
      map.put(i, stats);
    }
    return map;
  }
  
  /*
   * Source: http://stackoverflow.com/questions/3758606/how-to-convert-byte-size-into-human-readable-format-in-java
   */
  public static String humanReadableByteCount(long bytes, boolean si) {
    int unit = si ? 1000 : 1024;
    if (bytes < unit)
      return bytes + " B";
    int exp = (int) (Math.log(bytes) / Math.log(unit));
    String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
    return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
  }

}
