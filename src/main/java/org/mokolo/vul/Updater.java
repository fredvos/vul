package org.mokolo.vul;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.mokolo.vul.updater.SourceDirectory;
import org.mokolo.vul.updater.TargetCollection;
import org.mokolo.vul.updater.TargetDirectory;

public class Updater {
  private File configDirectory;
  private List<Collection> collections;
  private Map<String, Collection> collectionsMap;
  private List<SourceDirectory> sourceDirectories;
  private Map<String, TargetDirectory> targetDirectories;
  private Map<String, TargetCollection> targetCollections;
  private Priorities priorities;
  private int maxDeletes;
  private int maxCopies;
  private Random prng;

  /**
   *  Walks through sorted source directories from top to bottom.
   *  It points to the next directory we want to copy to a target.
   */
  private int topIndex;
  
  /**
   * Walks through sorted source directories from bottom to top.
   * it points to the next possible directory we want to delete from a target
   * if present there,
   */
  private int bottomIndex;
  
  /**
   * Counts the number of directories deleted while updating.
   * It is maximized to keep the number of updates on a full target limited.
   */
  private int deletes;
  

  /**
   * Counts the number of directories copied while updating.
   * It is maximized to keep the number of updates on an empty target limited.
   */
  private int copies;
  
  private static Logger logger = Logger.getLogger(Updater.class);

  public Updater(File configDirectory) {
    this.configDirectory = configDirectory;
    this.collections = new ArrayList<Collection>();
    this.collectionsMap = new HashMap<String, Collection>();
    this.sourceDirectories = new ArrayList<SourceDirectory>();
    this.targetDirectories = new HashMap<String, TargetDirectory>();
    this.targetCollections = new HashMap<String, TargetCollection>();
    this.priorities = new Priorities();
    File prioritiesFile = new File(this.configDirectory, "priorities");
    try {
      this.priorities.load(prioritiesFile);
    } catch (IOException e) {
      logger.error(e.getClass().getName()+" occurred. Msg="+e.getMessage());
    }
    this.prng = new Random();
    this.maxDeletes = 6;
    this.maxCopies = 40;
  }

  public void addCollection(Collection collection) {
    this.collections.add(collection);
    this.collectionsMap.put(collection.getId(), collection);
    
    if (collection.getType() == Collection.Type.TARGET) {
      for (Directory directory : collection.getDirectoriesCollection()) {
        TargetDirectory targetDirectory = new TargetDirectory();
        targetDirectory.directory = directory;
        targetDirectory.collectionId = collection.getId();
        this.targetDirectories.put(directory.directory, targetDirectory);
      }
      TargetCollection targetCollection = new TargetCollection();
      targetCollection.size = collection.getIndexSize();
      targetCollection.maxSize = collection.getMaxSize();
      this.targetCollections.put(collection.getId(), targetCollection);
    }
    else {
      for (Directory directory : collection.getDirectoriesCollection()) {
        SourceDirectory sourceDirectory = new SourceDirectory();
        sourceDirectory.directory = directory;
        sourceDirectory.collectionId = collection.getId();
        sourceDirectory.priority = this.randomize(0.0);
        int i = this.priorities.getPatternIndex(directory.directory);
        if (i >= 0) {
          sourceDirectory.priority = this.randomize(this.priorities.get(i).priority);
        }
        this.sourceDirectories.add(sourceDirectory);
      }
    }
  }

  public void update() throws DuplicateDirectoryException, InconsistentDirectoryException, IOException {
    logger.info("Verifying sources and targets");
    this.analyzeCollections();
    logger.info("Sorting source directories on descending randomized priorities");
    this.sortSourceDirectoriesOnDescendingRandomizedPriority();
    
    this.topIndex = 0;
    this.bottomIndex = this.sourceDirectories.size()-1;
    this.deletes = 0;
    this.copies = 0;
    
    while (this.topIndex < this.bottomIndex &&
           this.deletes < this.maxDeletes &&
           this.copies < this.maxCopies) {
      SourceDirectory sourceDirectory = this.sourceDirectories.get(this.topIndex);
      if (! this.targetDirectories.containsKey(sourceDirectory.directory.directory)) {
        logger.info("About to copy source directory '"+sourceDirectory.collectionId+":"+sourceDirectory.directory.directory+"'");
        String targetCollectionId = null;
        boolean deletedOne = false;
        do {
          targetCollectionId = this.findFirstTargetWithSpaceFor(sourceDirectory.directory.sizeEstimateBytes);
          if (targetCollectionId == null)
            deletedOne = this.deleteOneTargetDirectory();
        } while (targetCollectionId == null && deletedOne == true);
        if (targetCollectionId != null) {
          this.copyDirectory(sourceDirectory.directory.directory, sourceDirectory.collectionId, targetCollectionId);
        }
      }
      this.topIndex++;
    }
    
    logger.info("reindexing targets");
    this.reindexTargets();
    logger.info("Done");
  }
  
  private void analyzeCollections()
  throws DuplicateDirectoryException, InconsistentDirectoryException {
    Map<String, Directory> directories = new HashMap<String, Directory>();

    /*
     * Check for duplicates in sources:
     */
    for (Collection collection : this.collections) {
      if (collection.getType() == Collection.Type.SOURCE) {
        for (Directory directory : collection.getDirectoriesCollection()) {
          Directory directoryInMap = directories.get(directory.directory);
          if (directoryInMap != null)
            throw new DuplicateDirectoryException(
                "Directory '"+directory.directory+
                "' in collection '"+collection.getId()+"' is duplicate in source collections");
          directories.put(directory.directory, directory);
        }
      }
    }

    /*
     * Check for duplicates in targets:
     */
    directories.clear();
    for (Collection collection : this.collections) {
      if (collection.getType() == Collection.Type.TARGET) {
        for (Directory directory : collection.getDirectoriesCollection()) {
          Directory directoryInMap = directories.get(directory.directory);
          if (directoryInMap != null)
            throw new DuplicateDirectoryException(
                "Directory '"+directory.directory+
                "' in collection '"+collection.getId()+"' is duplicate in target collections"); 
          directories.put(directory.directory, directory);
        }
      }
    }
    
    /*
     * Check for inconsistencies:
     */
    directories.clear();
    for (Collection collection : this.collections) {
      for (Directory directory : collection.getDirectoriesCollection()) {
        Directory directoryInMap = directories.get(directory.directory);
        if (directoryInMap != null) {
          if (directory.compareTo(directoryInMap) != 0)
            throw new InconsistentDirectoryException(
                "Directory '"+directory.directory+
                "' is inconsistent with directory in other collection");
        }
        else
          directories.put(directory.directory, directory);
      }
    }

  }
  
  private void sortSourceDirectoriesOnDescendingRandomizedPriority() {
    Comparator<SourceDirectory> comparator = new Comparator<SourceDirectory>() {
      public int compare(SourceDirectory d1, SourceDirectory d2) {
        Double priority1 = new Double(d1.priority);
        Double priority2 = new Double(d2.priority);
        return priority2.compareTo(priority1);
      }
    };
    Collections.sort(this.sourceDirectories, comparator);
    logger.debug("Top sources after randomize and sort:");
    for (int i=0; i<3 && i<this.sourceDirectories.size(); i++) {
      logger.debug(String.format("%7.3f %s", this.sourceDirectories.get(i).priority, this.sourceDirectories.get(i).directory.directory));
    }
    logger.debug("Bottom sources after randomize and sort:");
    for (int i=Math.max(0, this.sourceDirectories.size()-3); i<this.sourceDirectories.size(); i++) {
      logger.debug(String.format("%7.3f %s", this.sourceDirectories.get(i).priority, this.sourceDirectories.get(i).directory.directory));
    }
  }

  private String findFirstTargetWithSpaceFor(long size) {
    String retval = null;
    for (Map.Entry<String, TargetCollection> entry : this.targetCollections.entrySet()) {
      if (entry.getValue().maxSize - entry.getValue().size >= size) {
        retval = entry.getKey();
        break;
      }
    }
    return retval;
  }
  
  private boolean deleteOneTargetDirectory() throws IOException {
    boolean deletedOne = false;
    do {
      SourceDirectory s = this.sourceDirectories.get(this.bottomIndex);
      TargetDirectory t = this.targetDirectories.get(s.directory.directory);
      if (t != null) {
        // Remove directory from file system:
        File directoryToDelete = new File(this.collectionsMap.get(t.collectionId).getBaseDirectory(), t.directory.directory);
        logger.info("Deleting directory '"+t.collectionId+":"+t.directory.directory+"'");
        if (! directoryToDelete.exists())
          throw new FileNotFoundException("Want to delete directory '"+directoryToDelete.getAbsolutePath()+"', but directory does not exist");
        this.deleteTargetDirectory(t.collectionId, s.directory.directory);
        // Remove directory from targetDirectories object:
        this.targetDirectories.remove(s.directory.directory);
        // Compute new size target:
        this.targetCollections.get(s.collectionId).size -= s.directory.sizeEstimateBytes;
        logger.info("New free space after delete = "+(this.targetCollections.get(s.collectionId).maxSize-this.targetCollections.get(s.collectionId).size));
        this.deletes++;
        if (this.deletes >= this.maxDeletes)
          logger.info("Maximum number of deleted reached");
        deletedOne = true;
      }
      this.bottomIndex--;
    } while (deletedOne == false && this.bottomIndex > this.topIndex);
    return deletedOne;
  }

  private void reindexTargets() throws IOException {
    for (Collection collection : this.collections) {
      if (collection.getType() == Collection.Type.TARGET) {
        collection.index();
      }
    }
  }

  /**
   * Deletes files in target directory and target directory too if empty.
   */
  private void deleteTargetDirectory(String collectionId, String directory)
  throws FileNotFoundException {
    File targetDirectory = new File(this.collectionsMap.get(collectionId).getBaseDirectory(), directory);
    File[] files = targetDirectory.listFiles();
    for (File file : files) {
      if (file.isFile())
        file.delete();
    }
    if (targetDirectory.listFiles().length == 0)
      targetDirectory.delete();
  }
  
  /**
   * Creates targetDirectory if necessary and copies files.
   * 
   * Non-recursive.
   */
  private void copyDirectory(String directory, String sourceCollectionId, String targetCollectionId)
  throws IOException {
    File sourceDirectory = new File(this.collectionsMap.get(sourceCollectionId).getBaseDirectory(), directory);
    File targetDirectory = new File(this.collectionsMap.get(targetCollectionId).getBaseDirectory(), directory);
    logger.info("Copying directory '"+sourceCollectionId+":"+directory+"' to "+
                "'"+targetCollectionId+":"+directory+"'");
    targetDirectory.mkdirs();
    File[] files = sourceDirectory.listFiles();
    for (File file : files) {
      if (file.isFile())
        FileUtils.copyFileToDirectory(file, targetDirectory);
    }
    this.copies++;
    if (this.copies >= this.maxCopies)
      logger.info("Maximum number of copies reached");
  }
  
  private double randomize(double base) {
    return base + this.prng.nextDouble() * 2.0 - 1.0;
  }
}
