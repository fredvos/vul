package org.mokolo.vul;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.DirectoryWalker;
import org.apache.log4j.Logger;

public class DirectoryCollector extends DirectoryWalker<Directory> {
  private File baseDirectory;
  private int baseDirectoryPathLength;
  
  private static int REPORT_EVERY = 100;
  private static Logger logger = Logger.getLogger(DirectoryCollector.class); 

  public DirectoryCollector(File baseDirectory) {
    super();
    this.baseDirectory = baseDirectory;
    this.baseDirectoryPathLength = this.baseDirectory.getAbsolutePath().length();
  }

  public List<Directory> collect() throws IOException {
    List<Directory> results = new ArrayList<Directory>();
    walk(this.baseDirectory, results);
    if (results.size() == 0 || results.size() % REPORT_EVERY != 0)
      logger.info("Indexed "+results.size()+" directories");
    return results;
  }

  @Override
  protected void handleDirectoryStart(File directory, int depth, Collection<Directory> results) {
    logger.debug("handleDirectoryStart()");
    Directory d = new Directory();
    logger.debug("Directory = "+directory.getAbsolutePath());
    if (directory.getAbsolutePath().equals(this.baseDirectory.getAbsolutePath()))
      d.directory = "."+File.separator;
    else
      d.directory = directory.getAbsolutePath().substring(this.baseDirectoryPathLength+1);
    logger.debug("Path = '"+d.directory+"'");
    File[] files = directory.listFiles();
    if (files != null) {
    for (File file : files) {
      if (file.isFile()) {
        if (isMusicFile(file))
          d.fileCount++;
        d.sizeEstimateBytes += file.length();
      }
    }
    }
    if (d.fileCount > 0) {
      results.add(d);
      if (results.size() % REPORT_EVERY == 0)
        logger.info("Indexed "+results.size()+" directories");
    }
  }
  
  public boolean isMusicFile(File file) {
    String filename = file.getName().toLowerCase();
    if (filename.endsWith(".mp3") ||
        filename.endsWith(".ogg") ||
        filename.endsWith(".wav") ||
        filename.endsWith(".flac"))
      return true;
    else
      return false;
  }

}
