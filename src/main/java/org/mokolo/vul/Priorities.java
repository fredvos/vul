package org.mokolo.vul;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

public class Priorities extends ArrayList<PatternPriority> {
  private static final long serialVersionUID = 2033757467684607833L;

  private static Logger logger = Logger.getLogger(Priorities.class);
  
  public void add(String patternString, double priority) {
    PatternPriority pp = new PatternPriority(patternString, priority);
    this.add(pp);
  }
  
  public void load(File prioritiesFile) throws IOException {
    BufferedReader reader = new BufferedReader(new FileReader(prioritiesFile));
    String line;
    while((line = reader.readLine()) != null) {
      try {
        line = line.trim();
        PatternPriority pp = PatternPriority.parseLine(line);
        this.add(pp);
      } catch (IsCommentException e) {
        // Ignore empty lines and lines starting with '#'
      } catch (org.mokolo.vul.ParsingException e) {
        logger.error("Cannot parse line '"+line+"'as priority. Msg="+e.getMessage());
      }
    }
    reader.close();
  }
  
  public int getPatternIndex(String path) {
    int index = -1;
    for (int i=0; i<this.size(); i++) {
      if (this.get(i).pattern.matcher(path).find() == true) {
        index = i;
        break;
      }
    }
    return index;
  }
  
}
