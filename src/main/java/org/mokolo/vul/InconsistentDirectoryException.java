package org.mokolo.vul;

public class InconsistentDirectoryException extends Exception {
  private static final long serialVersionUID = -3964504148009669866L;
  public InconsistentDirectoryException(String msg) {
    super(msg);
  }
}
