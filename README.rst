===
Vul
===

Fills or updates an MP3 device with a random selection of directories containing music files.

Can use multiple source directories and multiple target directories.

Use case
++++++++
I have a Sandisk Sansa Clip Zip 8 GB MP3 player with an additional 32 GB microSD card inserted.
Together this makes 40 GB.
I have more gigabytes of music stored in my NAS device at home than I can load in my MP3 player.
I like some artists or albums more than other artists or albums.
I need software that fills my MP3 player randomly with a selection of my collection,
but with a preference for music that I like.

When I connect my MP3 player with my desktop computer I see two USB devices being mounted, one for the internal 8 GB storage and one for the 32 GB microSD card.
Therefore I need multiple targets for vul.

Collections
+++++++++++
Each source and target is a collection of directories.
All collections have unique IDs.
Descriptions of collections are stored as Java property files as $HOME/.vul/default/<id>.collection
These files also contain information gathered during indexing.
Each collection needs a base directory declared.

**Where's the base directory?**

Each collection must have a base directory.
In the case of my collection on my NAS, this is a mounted directory at a fixed location.
Here I only need to provide the base directory when I create a new collection definition.

For the disks on my MP3 player things are different.
These are automatically mounted by Linux as /media/usb0 or /media/usb1 or /media/usb2 et cetera.
If my external USB disk for backups is mounted during startup, it is mounted as /media/usb0.
When I connect my MP3 player, the disks on that player get mounted as /media/usb1 and /media/usb2.
If my external USB disk is not mounted during startup (because it is not powered or whatever)
and I connect my MP3 player, the disks get mounted as /media/usb0 and /media/usb1.
This means there's no fixed path for the base directories of the two collections at the MP3 player.

In that case you must create a unique file in the base directory on the disk.
For the 8GB disk in my MP3 player I created an empty file called 'sansa8.vul' in its MUSIC directory.
For the 32GB disk I created a file called 'sansa32.vul' in the MUSIC directory.
Then you declare multiple base directories to the collection (i.e. /media/usb0/MUSIC, /media/usb1/MUSIC, /media/usb2/MUSIC, /media/usb3/MUSIC).
For the collection you define a check-filename.
For collection Sansa8 the check-filename was 'sansa8.vul'.

To detect the base directory of a collection, vul checks if a check-filename is set for the collection.
If not, the first (and probably only) base-directory declared, will be the base directory for the collection.
If a check-filename is set, vul checks all base directories for the collection until it finds one with the check-filename.

Files
+++++
All files are located in directory $HOME/.vul/default.

Collection definitions
----------------------
Files \*.collection

These are Java property files.
The string before '.collection' is the ID of the collection. 

Example (Sansa8.collection)::

  #Created by vul
  #Wed May 07 22:14:11 CEST 2014
  index-timestamp=2014-05-07 22\:14\:11
  index-directory-count=42
  check-filename=sansa8.vul
  base-directories=/media/usb0/MUSIC,/media/usb1/MUSIC,/media/usb2/MUSIC,/media/usb3/MUSIC
  type=t
  index-size=3676000800
  
Entries:

base-directories
  Required. Comma separated list of base directies. You can edit this entry if you need to.    
check-filename
  Optional. Use this to find the right base directory from the list. You can edit this entry if you need to.
index-timestamp
  Generated. Timestamp of last index. Do not edit. You can remove the entry if you deleted the index.
index-directory-count
  Generated. Number of directories with music files found. Do not edit. You can remove the entry if you deleted the index.
index-size
  Generated. Sum of file sizes in bytes of all files in directories with music files. Do not edit. You can remove the entry if you deleted the index.
max-size
  Optional. Maximum size in bytes you want the collection to be.
type
  Required. Collection is source (s) or target (t).

Indexes
-------
Files \*.index
The string before '.index' represents the ID of the collection. 

These are text files, generated by the vul 'index' command. 
Each line represents a directory that contains music files.
Each line consists of three 'columns', separated by spaces.
The first column is the number of music files.
The second column is the total size of all files in the directory in bytes, including non-music files.
The third column is the name of the directory. 

Priorities
----------
File priorities

Text file.
Each line contains pattern and a priority, the priority first, and then the pattern.

Example::

  2.0 Tinariwen/The_Radio_Tisdas_Sessions
  1.5 Tinariwen
  0.5 Blondie
  1.0 Nico
  -0.2 Earth.*Fire
  -0.5 Philip.Glass
  -0.5 Bach
  -2.0 Kora_Jazz_Trio
  1.0 The_Naked_And_Famous
  1.0 [Aa]fri[ck]a
  1.0 [Aa]rab
  -1.0 Deep\ Purple
  
Vul loads the file and compiles all patterns.
For regular expressions see http://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html
For sorting the collection on priority, each directory name is matched against each of the patterns, until it finds a match.
The priority found for the matching pattern is the 'mean' priority.
Then a random number between -1.0 and +1.0 is computed and added to this mean number.
This determines the priority of the directory for that run.
If no match is found, the 'mean' priority is 0.0.

So, if a directory matches a pattern with priority 0.5, this can result in a computed priority between -0.5 and +1.5.

Usage
+++++
``vul [flags] <command> [parameters]``

See below how to run.

Flags
-----
\-v
  Verbose output

Commands
--------
add-collection
  Adds collection (required parameters: id, type, base-directory; optional parameters: check-filename, max-size)
analyze-priorities
  Checks collection against current list of priorities and prints overview (required parameter: id)
list
  Lists collections
index
  (Re)indexes a collection (required parameter: id)
update
  Updates targets by adding or replacing directories; reindexes targets afterwards; number of deletes and number of copies is limited to 6 and 40 respectively (will be configurable in future)

Parameters
----------
base-directory
  A base directory of a collection
check-filename
  Verify base directory by checking if file with this name present
id
  Unique ID of collection
max-size
  Maximum size of collection in bytes; default is about 95% * (current size collection + free space)
type
  Collection type; s=source, t=target

Build Vul
+++++++++

- Make sure you forked my commons repository (https://github.com/fredvos/commons)
  and installed all its components to your local Maven repository ($ mvn install)
- Fork the Vul repository
- CD to the base directory, where pom.xml is located
- Build jar: ``$ mvn package`` (results in target/vul-0.1-SNAPSHOT-jar-with-dependencies.jar)

Run Vul
+++++++
After building Vul, run ``$ java -cp target/vul-0.1-jar-with-dependencies.jar org.mokolo.vul.CLI [flags] <command> [parameters]``

Of course you better create and use a script to run Vul.
If converted to and installed as a software package, such a script should be available in the PATH.
